# Medication labeler

## Installation
1. Click on this link: \\\wlad-isoserver\installers\Medication labeler
2. Double-click on setup.exe
3. Click Installation

## Usage
1. If all you need to do is print medications (don't need to add or modify), you don't need your own user added. 
  * If you *do* need to add or modify meds, you will need your own user - submit a ticket to it@nvih.org
2. Find the Medication Labeler icon on your desktop (or search your computer with the Start button and type 'medication')
3. Once it opens, you just fill in the information for your patient (first name/last name, account number, etc)
4. Pick your site and printer (medical/dental) and medication and make sure it all looks correct.
5. Print. If you need more than one label for the same med, hit print as many times as you need.
6. When you're done, hit clear.

## Adding medications
1. Hit 'Add'. Fill in your username and password at the top (you needed to contact IT to set these).
2. Fill in the information about your med; if that medication has already been added to inventory at some point, 
it'll be in the list. If not, just type in the name of your med.
3. 
